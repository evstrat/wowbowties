__author__ = 'Bogdan'
__email__ = 'evstrat.bg@gmail.com'
__mobile_phone__ = 89252608400

from flask import Flask, render_template


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/about.html')
def about():
    return render_template('index.html')


@app.route('/gallery.html')
def gallery():
    return render_template('index.html')


@app.route('/contact.html')
def contacts():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
